class PayrollSimulation {
 public static void main(String[] args) {
   Employee emp = new Employee();
   emp.name = "Mark";
   emp.wage = 100;
   emp.hours = 10;
   
   double emp_pay = emp.calculatePay(emp.hours, emp.wage);
   System.out.println(emp_pay);
   
   Manager mgr = new Manager();
   mgr.name = "Mark";
   mgr.wage = 100;
   mgr.hours = 10;
   mgr.bonus = 500;
   
   double mgr_pay = mgr.calculatePay(mgr.hours, mgr.wage, mgr.bonus);
   System.out.println(mgr_pay);
 }
}